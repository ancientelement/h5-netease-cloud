import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import ItemMusic from '@/views/ItemMusic.vue'
import Search from '@/views/Search.vue'
import Video from '@/views/Video.vue'
import PlayVideo from '@/views/PlayVideo.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/itemMusic/:id',
    name: 'ItemMusic',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: ItemMusic
  },
  {
    path: '/search',
    name: 'Search',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Search
  },
  {
    path: '/video',
    // name: 'Video', 父级路由不需要name属性
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Video,
  },
  { path: '/playvideo/:id?', name: PlayVideo, component: PlayVideo }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to,from,next) => {
  if(to.path.indexOf('/playvideo') != -1) {
    console.log(to.path);
    console.log("yes");
  }
  next();
}) 

export default router
