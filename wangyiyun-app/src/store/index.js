import { createStore } from 'vuex'
import service from '@/request'

export default createStore({
  state: {
    playList: [{ //播发列表
      al: {
        id: 35139,
        name: "追梦痴子心",
        pic: 19061133579343590,
        picUrl: "https://p1.music.126.net/XDncptlBJ4_LN3hLBx-8aw==/19061133579343591.jpg",
        pic_str: "19061133579343591",
      },
      ar: [{
        name: "GALA"
      }],
      name: "追梦赤子心",
      id: 355992,
    }],
    playListIndex: 0, // 当前播放的歌曲
    isPlay: false,//是否在播放
    detailShow: false, //详情页展示
    lyric: {}, //歌词
    currentTime: 0, //当前播放时间
    duration: 0, //歌曲时长
    footerShow: true, //底部组件是否展示
  },
  getters: {
  },
  mutations: {
    UPDATEISPLAY(state, value) { //更新播放状态
      state.isPlay = value
    },
    UPDATAEPLAYLIST(state, value) {  //更新播放列表
      state.playList = value
    },
    UPDATEPLAYLISTINDEX(state, value) { //更新 播放歌曲
      state.playListIndex = value
    },
    UPDATEDETAILSHOW(state, value) { //更新 歌曲详细显示
      state.detailShow = !state.detailShow
    },
    UPDATELYRIC(state, value) { //更新 歌词显示
      state.lyric = value
    },
    UPDATECURRENTTIME(state, value) { //更新 歌曲播放时间
      state.currentTime = value
    },
    UPDATEDURATION(state, value) { //更新 歌曲时长
      state.duration = value
    },
    ADDPLAYLIST(state, value) {  //添加 播放歌曲
      console.log(11);
      state.playList.push(value)
    },
    UPDATEFOOTERSHOW(state, value) {  //添加 播放歌曲
      state.footerShow = value
    }
  },
  actions: {
    async getLyric(context, value) {
      const res = await service.get(`/lyric?id=${value}`)
      // console.log("lyric", res);
      context.commit('UPDATELYRIC', res.data.lrc)
    }
  },
  modules: {
  }
})
