import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

//注册全局组件 FooterMusic
import FooterMusic from '@/components/item/FooterMusic.vue'


//注册全局组件 MusicDetails
import MusicDetails from '@/components/item/MusicDetails.vue' 

//全局样式与字体
import "@/assets/sass/style.scss"
import "@/assets/iconfont/iconfont.js"


console.log(123);

createApp(App).use(store).use(router).mount('#app')
