const express = require("express");

const app = express();

app.use('/',express.static(__dirname + "/dist"))
app.use('/uploads',express.static(__dirname + "/uploads"))

app.listen(8080,() => {
    console.log("app is running at http://localhost:8080");
})